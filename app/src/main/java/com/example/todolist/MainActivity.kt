package com.example.todolist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        plusButton.setOnClickListener {

        }
        minusButton.setOnClickListener {

        }
        addButton.setOnClickListener {
            addText()

        }
        text.setOnClickListener {

        }
        number.setOnClickListener {

        }
        editText.setOnClickListener {


        }
    }
    private fun addText() {
        text.text =number.text.toString()+"    "+ text.text.toString() + editText.text.toString()
    }
}
